
package com.mycompany.derecha;

import java_cup.runtime.Symbol;
import java.io.*;
import java.lang.*;

%%

par=[0,2,4,6,8]
impar=[1,3,5,7,9]
c_punto_coma=";"

%cup
%line
%char
%class Scanner

%%

{par} {return new Symbol(sym.par,new Token(sym.par, new String(yytext()), yyline, yychar));}
{impar} {return new Symbol(sym.impar, new Token(sym.impar, new String(yytext()), yyline, yychar));}
{c_punto_coma} {return new Symbol(sym.c_punto_coma, new Token(sym.c_punto_coma, new String(yytext()), yyline, yychar));}

[\b\t\r\f\n]+ {}
" "+ {}
. {System.out.println("Error Lexico Identificado "+ yytext() + " en la linea "+ yyline +" y posición " + yychar);}
